import ShoppingItem from './ShoppingItem';

describe('ShoppingItem', () => {
    it('should return product name', () => {
        // given
        const shoppingItem = new ShoppingItem('apple3');
        // when
        const result = shoppingItem.getName();
        // then
        expect(result).toBe('apple3');
    });
    it('should return total price', () => {
        // given
        const shoppingItem = new ShoppingItem('name', 4, 2.5);
        // when
        const result = shoppingItem.getTotalPrice();
        // then
        expect(result).toBe(10);
    });
});
