export default class Cart {
    constructor() {
        this.items = [];
    }

    add = shoppingItem => this.items.push(shoppingItem);

    getTotalPrice = (discount = price => 0) => {
        const totalPrice = this.items.reduce((result, shoppingItem) => result + shoppingItem.getTotalPrice(), 0);
        const priceWithDiscount = totalPrice - discount(totalPrice);
        return priceWithDiscount;
    };

    getVipItems = () => this.items.filter(item => item.getPrice() >= 20);

    getNames = () => this.items.map(item => item.getName());
}
