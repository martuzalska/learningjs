import ShoppingItem from './ShoppingItem';
import Cart from './Cart';


describe('Cart', () => {
    const discount = function (percent) {
        return function (totalPrice) {
            return totalPrice * percent;
        };
    };

    it('should return total cost of all products', () => {
        // given
        const cart = new Cart();
        cart.add(new ShoppingItem('apple', 3, 10));
        cart.add(new ShoppingItem('banana', 2, 20));
        const expectedResult = 70;
        // when
        const result = cart.getTotalPrice();
        // then
        expect(result).toBe(expectedResult);
    });
    it('should return all vip items', () => {
        // given
        const cart = new Cart();
        const apple = new ShoppingItem('apple', 3, 10);
        const banana = new ShoppingItem('banana', 2, 20);
        cart.add(apple);
        cart.add(banana);
        const expectedResult = [banana];
        // when
        const result = cart.getVipItems();
        // then
        expect(result).toEqual(expectedResult);
    });
    it('should return total cost with discount of all products', () => {
        // given
        const cart = new Cart();
        cart.add(new ShoppingItem('apple', 3, 10));
        cart.add(new ShoppingItem('banana', 2, 20));
        const expectedResult = 35;
        // when
        const result = cart.getTotalPrice(discount(0.5));
        // then
        expect(result).toBe(expectedResult);
    });
    it('should return names of all products', () => {
        // given
        const cart = new Cart();
        cart.add(new ShoppingItem('apple', 3, 10));
        cart.add(new ShoppingItem('banana', 2, 20));
        const expectedResult = ['apple', 'banana'];
        // when
        const result = cart.getNames();
        // then
        expect(result).toEqual(expectedResult);
    });
});
