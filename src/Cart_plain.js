
// getTotalPrice = function () {
//     let result = 0;
//     for ( var i=0; i<this.items.length;i++){
//         var item= this.items[i]
//
//         result +=item.getTotalPrice()
//     }
//
//     this.items.forEach(function(item){
//         result +=item.getTotalPrice();\
//     })
// };

//
// this.getVipItems = function () {
//     var result = [];
//     this.items.forEach(function (item) {
//         if (item.getPrice() >= 20) {
//             result.push(item)
//         }
//     })

//
// //
//
// getVipItems = function () {
//     return this.items.filter(item => item.getPrice() >= 20);
// };
//
//   reduce explanation
// ```this.items = [
//  new ShoppingItem('apple'),
//  new ShoppingItem('banana').
// ]
//
// getNames = () => this.items.reduce((result, item) => result.concat([item.getName()]), []);
// ```
//
// czyli jak tego reduce prześldzimy sobie krok po kroku, to będzie tak:
//     ```[new ShoppingItem('apple'), new ShoppingItem('banana')].reduce((result, item) => result.concat([item.getName()]), []);
// ```
//
//
//     ==>
//     ```[] => (result, item) => result.concat([item.getName()]);
//
// (result = [], item = ShoppingItem('apple')) => [].concat(['apple']);
// (result = ['apple'], item = ShoppingItem('banana')) => ['apple'].concat(['banana']);
//
// result = ['apple', 'banana'];```
